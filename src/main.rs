use glam::{DQuat, DVec3, EulerRot};
use image::{Rgba, RgbaImage};
use pest::{iterators::Pair, Parser};
use pest_derive::Parser;
use std::{ops::Range, path::PathBuf};

#[derive(Parser)]
#[grammar = "args.pest"]
struct Args;

fn escape(mut s: String) -> String {
	let mut last_checked = s.len();
	while let Some(i) = s[..last_checked].rfind(|c| [' ', '\\'].contains(&c)) {
		s.insert(i, '\\');
		last_checked = i;
	}
	s
}

fn unescape(mut s: String) -> String {
	let mut last_checked = 0;
	while let Some(i) = s[last_checked..].find('\\') {
		s.remove(i + last_checked);
		last_checked += i + 1;
	}
	s
}

#[derive(Debug)]
struct Program {
	ins: Vec<Input>,
	outs: Vec<Output>,
}

#[derive(Debug)]
struct Input {
	path: PathBuf,
	proj: Projection,
}

#[derive(Debug)]
struct Output {
	path: PathBuf,
	dim: Dimensions,
	proj: Projection,
}

#[derive(Debug)]
struct Dimensions {
	width: u32,
	height: u32,
}

#[derive(Debug)]
enum Projection {
	Equirectangular {
		orient: Orientation,
		width: f64,
		height: f64,
	},
	Stereographic {
		orient: Orientation,
		limit: f64,
	},
	Trimetric {
		base: [SphereCoords; 3],
		roll: f64,
		width: f64,
		height: f64,
	},
	AzimuthalEqualArea {
		orient: Orientation,
		limit: f64,
	},
}

#[derive(Debug)]
struct Orientation {
	lat: f64,
	lon: f64,
	roll: f64,
}

impl Orientation {
	fn normalize(&self) -> Self {
		Self {
			lat: SphereCoords::normalize_lat(self.lat),
			lon: SphereCoords::normalize_lon(self.lon),
			roll: SphereCoords::normalize_lon(self.roll),
		}
	}

	fn transform(&self, coords: &SphereCoords) -> SphereCoords {
		let cart = coords.to_cartiesian();
		let rotation = DQuat::from_euler(
			EulerRot::XYZ,
			-self.roll.to_radians(),
			self.lat.to_radians(),
			-self.lon.to_radians(),
		);
		let transformed = rotation * cart;
		SphereCoords::from_cartesian(&transformed)
	}

	fn inv_transform(&self, coords: &SphereCoords) -> SphereCoords {
		let cart = coords.to_cartiesian();
		let rotation = DQuat::from_euler(
			EulerRot::ZYX,
			self.lon.to_radians(),
			-self.lat.to_radians(),
			self.roll.to_radians(),
		);
		let transformed = rotation * cart;
		SphereCoords::from_cartesian(&transformed)
	}
}

#[derive(Clone, Debug, PartialEq)]
struct SphereCoords {
	lat: f64,
	lon: f64,
}

impl SphereCoords {
	fn normalize(&self) -> Self {
		Self {
			lat: Self::normalize_lat(self.lat),
			lon: Self::normalize_lon(self.lon),
		}
	}

	fn normalize_lat(mut lat: f64) -> f64 {
		lat = lat.rem_euclid(360.0);
		if lat >= 180.0 {
			lat -= 360.0;
		}
		if lat.abs() > 90.0 {
			lat = lat.signum() * 180.0 - lat;
		}
		lat
	}

	fn normalize_lon(mut lon: f64) -> f64 {
		lon = lon.rem_euclid(360.0);
		if lon >= 180.0 {
			lon -= 360.0;
		}
		lon
	}

	fn to_cartiesian(&self) -> DVec3 {
		let (sin, cos) = (f64::sin, f64::cos);
		DVec3 {
			x: cos(self.lon.to_radians()) * cos(self.lat.to_radians()),
			y: sin(self.lon.to_radians()) * cos(self.lat.to_radians()),
			z: sin(self.lat.to_radians()),
		}
	}

	fn from_cartesian(cart: &DVec3) -> Self {
		let (asin, atan2) = (f64::asin, f64::atan2);
		Self {
			lon: atan2(cart.y, cart.x).to_degrees(),
			lat: asin(cart.z / cart.length()).to_degrees(),
		}
	}

	/// Distance in degrees between self and other
	fn dist(&self, other: &SphereCoords) -> f64 {
		fn cos(deg: f64) -> f64 {
			f64::cos(deg.to_radians())
		}

		fn hav(deg: f64) -> f64 {
			(1.0 - cos(deg.to_radians())) / 2.0
		}

		let central_angle =
			hav(other.lat - self.lat) + cos(self.lat) * cos(other.lat) * hav(other.lon - self.lon);

		central_angle.to_degrees()
	}
}

impl From<Pair<'_, Rule>> for Program {
	fn from(pair: Pair<Rule>) -> Self {
		assert!(pair.as_rule() == Rule::args);

		let mut prog = Self {
			ins: Vec::new(),
			outs: Vec::new(),
		};

		for pair in pair.into_inner() {
			match pair.as_rule() {
				Rule::input => prog.ins.push(Input::from(pair)),
				Rule::output => prog.outs.push(Output::from(pair)),
				_ => (),
			}
		}

		prog
	}
}

impl From<Pair<'_, Rule>> for Input {
	fn from(pair: Pair<Rule>) -> Self {
		assert!(pair.as_rule() == Rule::input);

		let mut pairs = pair.into_inner();

		Self {
			proj: Projection::from(pairs.next().unwrap()),
			path: PathBuf::from(unescape(pairs.next().unwrap().as_str().to_string())),
		}
	}
}

impl From<Pair<'_, Rule>> for Output {
	fn from(pair: Pair<Rule>) -> Self {
		assert!(pair.as_rule() == Rule::output);

		let mut pairs = pair.into_inner();

		Self {
			proj: Projection::from(pairs.next().unwrap()),
			dim: Dimensions::from(pairs.next().unwrap()),
			path: PathBuf::from(unescape(pairs.next().unwrap().as_str().to_string())),
		}
	}
}

impl From<Pair<'_, Rule>> for Dimensions {
	fn from(pair: Pair<Rule>) -> Self {
		assert!(pair.as_rule() == Rule::dimensions);

		let mut pairs = pair.into_inner();

		Self {
			width: pairs
				.next()
				.unwrap()
				.as_str()
				.parse()
				.expect("Expected int"),
			height: pairs
				.next()
				.unwrap()
				.as_str()
				.parse()
				.expect("Expected int"),
		}
	}
}

impl From<Pair<'_, Rule>> for Projection {
	fn from(pair: Pair<Rule>) -> Self {
		use Projection::*;

		assert!(pair.as_rule() == Rule::projection);

		let (inner_rule, mut pairs) = {
			let inner = pair.into_inner().next().unwrap();
			(inner.as_rule(), inner.into_inner())
		};

		match inner_rule {
			Rule::equirectangular => Equirectangular {
				orient: Orientation::from(pairs.next().unwrap()),
				height: pairs
					.next()
					.unwrap()
					.as_str()
					.parse()
					.expect("Expected float"),
				width: pairs
					.next()
					.unwrap()
					.as_str()
					.parse()
					.expect("Expected float"),
			},
			Rule::stereographic => Stereographic {
				orient: Orientation::from(pairs.next().unwrap()),
				limit: pairs
					.next()
					.unwrap()
					.as_str()
					.parse()
					.expect("Expected float"),
			},
			Rule::trimetric => Trimetric {
				base: [
					SphereCoords::from(pairs.next().unwrap()),
					SphereCoords::from(pairs.next().unwrap()),
					SphereCoords::from(pairs.next().unwrap()),
				],
				roll: pairs
					.next()
					.unwrap()
					.as_str()
					.parse()
					.expect("Expected float"),
				height: pairs
					.next()
					.unwrap()
					.as_str()
					.parse()
					.expect("Expected float"),
				width: pairs
					.next()
					.unwrap()
					.as_str()
					.parse()
					.expect("Expected float"),
			},
			Rule::azimuthal_equal_area => AzimuthalEqualArea {
				orient: Orientation::from(pairs.next().unwrap()),
				limit: pairs
					.next()
					.unwrap()
					.as_str()
					.parse()
					.expect("Expected float"),
			},
			r => panic!("Unexpected projecion type: {r:?}"),
		}
	}
}

impl From<Pair<'_, Rule>> for SphereCoords {
	fn from(pair: Pair<Rule>) -> Self {
		assert!(pair.as_rule() == Rule::coords);

		let mut pairs = pair.into_inner();

		Self {
			lat: pairs
				.next()
				.unwrap()
				.as_str()
				.parse()
				.expect("Expected float"),
			lon: pairs
				.next()
				.unwrap()
				.as_str()
				.parse()
				.expect("Expected float"),
		}
	}
}
impl From<Pair<'_, Rule>> for Orientation {
	fn from(pair: Pair<Rule>) -> Self {
		assert!(pair.as_rule() == Rule::orientation);

		let mut pairs = pair.into_inner();

		Self {
			lat: pairs
				.next()
				.unwrap()
				.as_str()
				.parse()
				.expect("Expected float"),
			lon: pairs
				.next()
				.unwrap()
				.as_str()
				.parse()
				.expect("Expected float"),
			roll: pairs
				.next()
				.unwrap()
				.as_str()
				.parse()
				.expect("Expected float"),
		}
	}
}

impl From<(u32, u32)> for Dimensions {
	fn from((width, height): (u32, u32)) -> Self {
		Self { width, height }
	}
}

#[derive(Debug, PartialEq)]
struct Point {
	x: f64,
	y: f64,
}

impl Projection {
	/// Project from spherical coords to normalized map coords
	fn project(&self, coords: &SphereCoords) -> Option<Point> {
		use std::f64::consts::*;
		use Projection::*;
		match self {
			Equirectangular {
				orient,
				width,
				height,
			} => {
				let coords = orient.transform(coords);

				let min_lat = -height / 2.0;
				let max_lat = height / 2.0;
				let min_lon = -width / 2.0;
				let max_lon = width / 2.0;

				let x = map_range(coords.lon, min_lon..max_lon, 0.0..1.0);
				let y = map_range(coords.lat, max_lat..min_lat, 0.0..1.0);

				Some(Point { x, y })
			}
			Stereographic { orient, limit } => {
				let coords = Orientation {
					lat: -90.0,
					lon: 0.0,
					roll: 0.0,
				}
				.transform(&orient.transform(coords));

				let scale = f64::tan(FRAC_PI_4 - (FRAC_PI_2 - limit.to_radians()) / 2.0);
				let r = f64::tan(FRAC_PI_4 - coords.lat.to_radians() / 2.0) / scale;
				let theta = coords.lon.to_radians() - FRAC_PI_2;

				let (s, c) = f64::sin_cos(theta);
				let x = 0.5 + r * c / 2.0;
				let y = 0.5 - r * s / 2.0;

				Some(Point { x, y })
			}
			Trimetric {
				base,
				roll,
				width,
				height,
			} => {
				let dists = base.clone().map(|base_coords| base_coords.dist(&coords));

				unimplemented!()
			}
			AzimuthalEqualArea { orient, limit } => {
				let coords = orient.transform(coords);
				let cart = coords.to_cartiesian();

				let scale = 2.0 * f64::cos((PI - limit.to_radians()) / 2.0);
				let x = cart.y * f64::sqrt(2.0 / (cart.x - 1.0)) / scale;
				let y = cart.z * f64::sqrt(2.0 / (cart.x - 1.0)) / scale;

				let x_map = (x + 1.0) / 2.0;
				let y_map = (y - 1.0) / -2.0;

				Some(Point { x: x_map, y: y_map })
			}
		}
	}

	/// Project from normalized map coords to spherical coords
	fn inv_project(&self, point: &Point) -> Option<SphereCoords> {
		use std::f64::consts::*;
		use Projection::*;
		match self {
			Equirectangular {
				orient,
				width,
				height,
			} => {
				let min_lat = -height / 2.0;
				let max_lat = height / 2.0;
				let min_lon = -width / 2.0;
				let max_lon = width / 2.0;

				let lat = map_range(point.y, 1.0..0.0, min_lat..max_lat);
				let lon = map_range(point.x, 0.0..1.0, min_lon..max_lon);

				Some(orient.inv_transform(&SphereCoords { lat, lon }))
			}
			Stereographic { orient, limit } => {
				let x = map_range(point.x, 0.0..1.0 as f64, -1.0..1.0);
				let y = map_range(point.y, 0.0..1.0 as f64, 1.0..-1.0);

				let r = f64::sqrt(x * x + y * y);
				let theta = f64::atan2(y, x);

				let scale = f64::tan(FRAC_PI_4 - (FRAC_PI_2 - limit.to_radians()) / 2.0);
				let lat = ((FRAC_PI_4 - f64::atan(r * scale)) * 2.0).to_degrees();
				let lon = theta.to_degrees() + 90.0;

				Some(
					orient.inv_transform(
						&Orientation {
							lat: -90.0,
							lon: 0.0,
							roll: 0.0,
						}
						.inv_transform(&SphereCoords { lat, lon }),
					),
				)
			}
			Trimetric {
				base,
				roll,
				width,
				height,
			} => {
				unimplemented!()
			}
			AzimuthalEqualArea { orient, limit } => {
				let scale = 2.0 * f64::cos((PI - limit.to_radians()) / 2.0);
				let x = (point.x * 2.0 - 1.0) * scale;
				let y = (point.y * -2.0 + 1.0) * scale;

				let r_squared = x * x + y * y;

				let cart = DVec3 {
					x: 1.0 - r_squared / 2.0,
					y: x * f64::sqrt(1.0 - r_squared / 4.0),
					z: y * f64::sqrt(1.0 - r_squared / 4.0),
				};

				let coords = SphereCoords::from_cartesian(&cart);

				Some(orient.inv_transform(&coords))
			}
		}
	}
}

fn main() {
	let cmd_line = std::env::args()
		.skip(1)
		.map(escape)
		.collect::<Vec<_>>()
		.join(" ");
	let mut pairs = Args::parse(Rule::args, &cmd_line).unwrap_or_else(|e| {
		eprintln!("{e}");
		std::process::exit(1);
	});
	let prog = Program::from(pairs.next().unwrap());

	let in_images = prog
		.ins
		.iter()
		.map(|input| {
			image::open(&input.path)
				.unwrap_or_else(|e| panic!("failed to open image: {e}"))
				.into_rgba8()
		})
		.collect::<Vec<_>>();

	let out_images = reproject_all(&prog, &in_images);

	for (path, image) in prog.outs.iter().map(|out| &out.path).zip(out_images) {
		image
			.save(path)
			.unwrap_or_else(|e| panic!("failed to save {}: {e}", path.display()));
	}
}

fn reproject_all(prog: &Program, in_images: &Vec<RgbaImage>) -> Vec<RgbaImage> {
	prog.outs
		.iter()
		.map(|output| reproject(&prog.ins, output, &in_images))
		.collect()
}

fn reproject(ins: &Vec<Input>, output: &Output, in_images: &Vec<RgbaImage>) -> RgbaImage {
	let mut out_image = RgbaImage::new(output.dim.width, output.dim.height);

	let (width, height) = out_image.dimensions();
	for y in 0..height {
		for x in 0..width {
			let out_point = Point {
				x: x as f64 / width as f64,
				y: y as f64 / height as f64,
			};
			if let Some(coord) = output.proj.inv_project(&out_point) {
				for (input, in_image) in ins.iter().zip(in_images) {
					if let Some(in_point) = input.proj.project(&coord.normalize()) {
						if let Some(color) = sample_lanczos(in_image, &in_point, 4.0) {
							out_image[(x, y)] = color;
							break;
						}
					}
				}
			}
		}
	}

	out_image
}

/// Samples an image at normalized coordinates
fn sample_nearest(image: &RgbaImage, point: &Point) -> Option<Rgba<u8>> {
	let width = image.width() as f64 - 1.0;
	let height = image.height() as f64 - 1.0;
	let x = f64::round(point.x * width);
	let y = f64::round(point.y * height);
	if (0.0..=width).contains(&x) && (0.0..=height).contains(&y) {
		Some(image[(x as u32, y as u32)])
	} else {
		None
	}
}

fn sinc(x: f64) -> f64 {
	if x == 0.0 {
		1.0
	} else {
		f64::sin(x) / x
	}
}

/// Samples an image at normalized coordinates
fn sample_lanczos(image: &RgbaImage, point: &Point, radius: f64) -> Option<Rgba<u8>> {
	use std::f64::consts::PI;

	let width = image.width() as f64 - 1.0;
	let height = image.height() as f64 - 1.0;
	let x_center = point.x * width;
	let y_center = point.y * height;

	let x_min = f64::ceil(x_center - radius) as i32;
	let x_max = f64::floor(x_center + radius) as i32;

	let y_min = f64::ceil(y_center - radius) as i32;
	let y_max = f64::floor(y_center + radius) as i32;

	let mut color = [0.0, 0.0, 0.0, 0.0];
	for r in y_min..=y_max {
		for c in x_min..=x_max {
			if (0..image.width() as i32).contains(&c) && (0..image.height() as i32).contains(&r) {
				let x_offset = c as f64 - x_center;
				let y_offset = r as f64 - y_center;
				let x_fac = sinc(PI * x_offset) * sinc(PI * x_offset / radius);
				let y_fac = sinc(PI * y_offset) * sinc(PI * y_offset / radius);
				let fac = x_fac * y_fac;

				let samp = image[(c as u32, r as u32)].0;

				for channel in 0..4 {
					color[channel] += samp[channel] as f64 * fac;
				}
			}
		}
	}

	Some(Rgba::from(color.map(|x| x.round() as u8)))
}

fn map_range(x: f64, in_range: Range<f64>, out_range: Range<f64>) -> f64 {
	(x - in_range.start) / (in_range.end - in_range.start) * (out_range.end - out_range.start)
		+ out_range.start
}

#[cfg(test)]
mod test {
	use super::*;

	const APPROX_EQUAL_THRESHOLD: f64 = 0.0000001;

	#[test]
	fn test_orientation() {
		let rotated = Orientation {
			lat: 0.0,
			lon: 0.0,
			roll: 180.0,
		}
		.transform(&SphereCoords { lat: 0.0, lon: 0.0 });
		let expected = SphereCoords { lat: 0.0, lon: 0.0 };
		assert!(f64::abs(rotated.lat - expected.lat) < APPROX_EQUAL_THRESHOLD);
		assert!(f64::abs(rotated.lon - expected.lon) < APPROX_EQUAL_THRESHOLD);
	}

	#[test]
	fn test_equirectangular() {
		let projected = Projection::Equirectangular {
			orient: Orientation {
				lat: 0.0,
				lon: 90.0,
				roll: 0.0,
			},
			width: 360.0,
			height: 180.0,
		}
		.project(&SphereCoords { lat: 0.0, lon: 0.0 })
		.unwrap();
		let expected = Point { x: 0.25, y: 0.5 };
		assert!(f64::abs(projected.x - expected.x) < APPROX_EQUAL_THRESHOLD);
		assert!(f64::abs(projected.y - expected.y) < APPROX_EQUAL_THRESHOLD);
	}

	#[test]
	fn test_stereographic() {
		let projected = Projection::Stereographic {
			orient: Orientation {
				lat: 90.0,
				lon: 0.0,
				roll: 0.0,
			},
			limit: 90.0,
		}
		.project(&SphereCoords { lat: 0.0, lon: 0.0 })
		.unwrap();
		let expected = Point { x: 0.5, y: 1.0 };
		assert!(f64::abs(projected.x - expected.x) < APPROX_EQUAL_THRESHOLD);
		assert!(f64::abs(projected.y - expected.y) < APPROX_EQUAL_THRESHOLD);
	}
}
